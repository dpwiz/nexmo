-- | Read this fine manual: <https://docs.nexmo.com/>
-- 
-- DIY sms gateway: 
-- 
-- > import qualified Data.Text as T
-- > import           System.Environment (getArgs, getEnv)
-- > import           Nexmo (init, query, sendUnicode)
-- > 
-- > main :: IO ()
-- > main = do
-- >     (from:to:text) <- getArgs
-- >     let body = T.intercalate (T.singleton ' ') (map T.pack text)
-- > 
-- >     key <- getEnv "NEXMO_KEY"
-- >     secret <- getEnv "NEXMO_SECRET"
-- >     n <- Nexmo.init key secret
-- > 
-- >     Nexmo.query n $ sendUnicode (T.pack from) (read to) body
-- >     return ()

module Nexmo
    ( init, query
    , sendText, sendUnicode
    ) where

import Prelude hiding (init)
import Nexmo.API as Nexmo (init, query)
import Nexmo.SMS as Nexmo (sendText, sendUnicode)
