module Nexmo.Account
    ( GetBalance(..)
    , GetPricing(..)
    , Search(..)
    ) where

import Data.ByteString.Char8 (pack)
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8)
import Text.Printf (printf)

import Nexmo.API

-- | Retrieve your current account balance.
data GetBalance = GetBalance

instance NexmoRequest GetBalance where
    queryUrl _ = "account/get-balance"
    queryMethod _ = "GET"
    queryArgs _ = []

-- | Retrieve outbound pricing for a given country.
data GetPricing = CountryPricing Char Char
                | PrefixPricing Int

instance NexmoRequest GetPricing where
    queryUrl (CountryPricing _ _) = "account/get-pricing/outbound"
    queryUrl (PrefixPricing _) = "account/get-prefix-pricing/outbound"

    queryMethod _ = "GET"

    queryArgs (CountryPricing a b) = [("country", pack [a, b])]
    queryArgs (PrefixPricing pr) = [("prefix", pack $ show pr)]

-- | Search sent messages.
data Search = SearchMessage Text -- ^ By message ID
            | SearchMany [Text] -- ^ By multiple message IDs
            | SearchDate (Integer, Int, Int) Integer -- ^ By date (y, m, d) and recipient number.

instance NexmoRequest Search where
    queryUrl _ = "search/messages"
    queryMethod _ = "GET"

    queryArgs (SearchMessage id) = [("id", encodeUtf8 id)]

    queryArgs (SearchMany ids) = [ ("ids", encodeUtf8 id)
                                 | id <- ids
                                 ]

    queryArgs (SearchDate (y, m, d) rec) = [ ("date", pack $ printf "%d-%d-%d" y m d)
                                           , ("to", pack $ show rec)
                                           ]
