module Nexmo.API
    ( query
    , Nexmo(..), init
    , NexmoRequest(..)
    ) where

import Prelude hiding (init)
import Network.HTTP.Client
import Network.HTTP.Client.OpenSSL (withOpenSSL, opensslManagerSettings)
import Network.HTTP.Types (Method, renderSimpleQuery)
import qualified OpenSSL.Session as SSL
import Data.Monoid
import Data.Maybe
import Data.ByteString.Char8 (ByteString, pack)
import Data.Aeson (Value, decode)

-- | API session container.
-- Initializing HTTPS cerficates takes lots of time
-- so this better be done once.
data Nexmo = Nexmo { apiKey :: ByteString
                   , apiSecret :: ByteString
                   , apiManager :: Manager
                   }

-- | Initialize an API session.
--
-- > nexmo <- Nexmo.init "api_key" "api_secret"
init :: String -> String -> IO Nexmo
init key secret = withOpenSSL $ fmap nexmo manager
    where
        nexmo = Nexmo (pack key) (pack secret)

        sslContext = do
            ctx <- SSL.context
            SSL.contextSetDefaultCiphers ctx
            return ctx

        manager = newManager $ opensslManagerSettings sslContext

-- | Generic API request.
class NexmoRequest a where
    queryUrl :: a -> String
    queryMethod :: a -> Method
    queryArgs :: a -> [(ByteString, ByteString)]
    
-- | Invoke API query.
--
-- > nexmo `query` sendText from to text
query :: NexmoRequest a => Nexmo -> a -> IO Value
query (Nexmo{..}) request = do
    req <- parseUrl ("https://rest.nexmo.com/" <> queryUrl request)
    let args = ("api_key", apiKey)
             : ("api_secret", apiSecret)
             : queryArgs request

    let req' = case queryMethod request of
                   "GET" -> req { queryString = renderSimpleQuery False args }
                   _ -> urlEncodedBody args req

    res <- withOpenSSL $ httpLbs req' apiManager
    return . fromMaybe (error "Error parsing Nexmo Response body.")
           . decode
           $ responseBody res
