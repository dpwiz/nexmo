module Nexmo.TH
       ( dashName
       ) where

import Data.Char ( toLower )
import Text.Inflections (dasherize, parseCamelCase)
import Text.Inflections.Parse.Types as I (Word(..))

lowerWord :: I.Word -> I.Word
lowerWord (Word s)    = I.Word $ map toLower s
lowerWord (Acronym s) = Acronym $ map toLower s

-- | Generate an dasherized JSON key from camelCased field names.
dashName :: String -> String
dashName = dasherize
         . map lowerWord
         . either (error "dashName: Left value") id
         . parseCamelCase []
