{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TemplateHaskell #-}

module Nexmo.SMS
    ( sendText
    , sendUnicode
    , SendSMS(..)
    , SMSPayload(..)
    , SMSResponse(..)
    , SMSMessage(..)
    ) where

import Data.Aeson.TH
    (deriveFromJSON, Options(..), defaultOptions)
import Data.ByteString.Char8 (pack)
import Data.Default
import Data.Maybe
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8)

import Nexmo.API (NexmoRequest(..))
import Nexmo.TH (dashName)

import qualified Data.ByteString as BS




-- | SMS content.
data SMSPayload = TextSMS Text
                | UnicodeSMS Text
                | VCardSMS BS.ByteString
                | VCalSMS BS.ByteString
                | BinarySMS { binBody :: BS.ByteString
                            , binUDH  :: BS.ByteString
                            }
                deriving (Show)

-- | SMS sending request.
data SendSMS =
    SendSMS { smsFrom         :: Text
            , smsTo           :: Integer
            , smsPayload      :: SMSPayload
            , smsStatusReport :: Bool
            , smsClientRef    :: Maybe Text
            , smsNetworkCode  :: Maybe Text
            , smsTTL          :: Maybe Integer
            , smsClass        :: Maybe Int
            } deriving (Show)

data SMSResponse =
    SMSResponse
    { srespMessageCount :: !Text
    , srespMessages     :: ![SMSMessage]
    } deriving (Show, Eq, Ord)


data SMSMessage =
    SMSMessage
    { smessStatus           :: !Text
    , smessMessageId        :: !(Maybe Text)
    , smessTo               :: !(Maybe Text)
    , smessRemainingBalance :: !(Maybe Text)
    , smessMessagePrice     :: !(Maybe Text)
    , smessNetwork          :: !(Maybe Text)
    , smessErrorText        :: !(Maybe Text)
    } deriving (Show, Eq, Ord)

$(deriveFromJSON
  defaultOptions { fieldLabelModifier = dashName . drop 5
                 , omitNothingFields = True }
  ''SMSResponse)

$(deriveFromJSON
  defaultOptions { fieldLabelModifier = dashName . drop 5
                 , omitNothingFields = True }
  ''SMSMessage)


instance Default SendSMS where
    def = SendSMS
         { smsFrom         = ""
         , smsTo           = 0
         , smsPayload      = TextSMS ""
         , smsStatusReport = False
         , smsClientRef    = Nothing
         , smsNetworkCode  = Nothing
         , smsTTL          = Nothing
         , smsClass        = Nothing
         }

instance NexmoRequest SendSMS where
    queryUrl _ = "sms/json"
    queryMethod _ = "POST"
    queryArgs SendSMS{..} = catMaybes
        [ Just ("from", encodeUtf8 smsFrom)
        , Just ("to", pack $ show smsTo)
        , Just ("status-report-req", if smsStatusReport then "1" else "0")
        , fmap (\r -> ("client-ref", encodeUtf8 r))     smsClientRef
        , fmap (\c -> ("network-code", encodeUtf8 c))   smsNetworkCode
        , fmap (\t -> ("ttl", pack $ show t))           smsTTL
        , fmap (\b -> ("message-class", pack $ show b)) smsClass
        ] ++ payloadArgs smsPayload


payloadArgs :: SMSPayload -> [(BS.ByteString, BS.ByteString)]
payloadArgs (TextSMS t) = [ ("type", "text")
                          , ("text", encodeUtf8 t)
                          ]
payloadArgs (UnicodeSMS t) = [ ("type", "unicode")
                             , ("text", encodeUtf8 t)
                             ]

-- | Send an ASCII-only sms. Does no conversion and all fancy characters get translated to ?
sendText :: Text -> Integer -> Text -> SendSMS
sendText f t text = def { smsFrom = f
                        , smsTo = t
                        , smsPayload = TextSMS text
                        }

-- | Send Unicode sms. Safer, but each sms length is limited to ~60 characters.
sendUnicode :: Text -> Integer -> Text -> SendSMS
sendUnicode f t text = def { smsFrom = f
                           , smsTo = t
                           , smsPayload = UnicodeSMS text
                           }
