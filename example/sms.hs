import qualified Data.Text as T
import           System.Environment (getArgs, getEnv)
import           Nexmo (init, query, sendUnicode)

main :: IO ()
main = do
    (from:to:text) <- getArgs
    let body = T.intercalate (T.singleton ' ') (map T.pack text)

    key <- getEnv "NEXMO_KEY"
    secret <- getEnv "NEXMO_SECRET"
    n <- Nexmo.init key secret

    Nexmo.query n $ sendUnicode (T.pack from) (read to) body
    return ()
